# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()


@app.route('/')
def books():

    tytuly =  db.titles()
    return  render_template('book_list.html',booksTitles = tytuly)
    pass0


@app.route('/book/<book_id>/')
def book(book_id):

    ksiazka = db.title_info(book_id)
    print ksiazka
    return render_template('book_detail.html',ksiazka = ksiazka)
    pass


if __name__ == '__main__':
    app.run(debug=True)