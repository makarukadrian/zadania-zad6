# -*- coding: utf-8 -*-
import bottle
import sqlite3
import beaker.middleware
from contextlib import closing
from bottle import route, run, template, install, post, request, redirect, hook
from beaker.session import Session


#################
#  Konfiguracja #
#################

session_opts = {
    'session.type': 'file',
    'session.data_dir': './sesja/',
    'session.cookie_expires': 300,
    'session.auto': True,
    }

app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)

Username = 'admin'
Password = 'admin'


#############
# Aplikacja #
#############

@route('/')
def layout():
    posty = posts()
    session = request.environ.get('beaker.session')
    return template ('layout.html', posty=posty, msg='Witaj na blogu, microblogu',session=session)
@route('/login')
def login():
    return template('login.html', msg='Zaloguj sie')

@route('/login','POST')
def login():
    username = request.forms.get('username')
    password = request.forms.get('password')
    if username == Username and Password == password:
        session = request.environ.get('beaker.session')
        session['login'] = True
        session.save()
        redirect('/~p11/wsgi/')    
    else:
        return template('login.html', msg='Error')
@route ('/add')
def add():
    session = request.environ.get('beaker.session')
    if session.get('login'):
        return template('add.html', msg='Dodaj post')
    else:
        return template('login.html', msg='Zaloguj sie')
@route('/add','POST')
def add():
    Tytul = request.forms.get('title')
    Tresc = request.forms.get('text')
    if Tytul.strip() == "" or Tresc.strip() == "":
        return template('add.html', msg='Error')
    else:
        with closing(connect_db()) as db:
            db.text_factory = str
            db.execute("INSERT INTO posts (title, text) VALUES(?, ?)", [Tytul, Tresc])
            db.commit()
        redirect('/~p11/wsgi/')

@route('/wyloguj')
def logout():
    session = request.environ.get('beaker.session')
    session.delete()
    redirect('/~p11/wsgi/')
   
       

###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect('microblog.db')

def posts():
    with closing(connect_db()) as db:
        posty = db.execute('SELECT id, title, text FROM posts order by id desc')
        return [dict(id=row[0], title=row[1], text=row[2]) for row in posty]

def init_db():
    
        db = sqlite3.connect('microblog.db')
        with open('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
        db.close()
################
# Uruchomienie #
################

if __name__ == '__main__':
    bottle.run(app=app, host='194.29.175.240', port=44466)
