drop table if exists posts;
create table posts (
    id integer primary key autoincrement,
    title string not null,
    text string not null
);